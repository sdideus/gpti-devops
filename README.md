# Gestão e Planejamento de TI

## Tópicos Emergentes - DevOps

### Material de Apoio

#### Aula 01 - 05/03/2022 
  <a href="https://pt.slideshare.net/saulolopes79/tpicos-emergentes-devops" target="_blank">Tópicos Emergentes - DevOps - Apresentação de Slides </a>

### Lista de Presentes
  1. Saulo
  2. Vishnu
  3. Alexandre 
  4. Matheus
  5. Christian
  6. Pablo de la Fuente
  7. Fabio
  8. Carlos Moura
  9. Gustavo
  10. Paulo
  11. Marcelo Graciano
  12. Matheus Malta Pedroso
  13. Carlos Henrique Moura
  14. Johny
  15. Gabriel
  16. Yan
  17. Gilberto
